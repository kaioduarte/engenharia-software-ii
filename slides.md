<!-- page_number: true -->
<!-- $size: 16:9 -->

<style>
  .purple {
  	color: #84139e !important;
  }
  
  .gray {
  	color: #8c8c8c !important;
  }
  
  .text-center {
  	display: flex;
    align-items: center;
    justify-content: center;
  }
  
  .title {	
    font-size: 80px !important;
  }
  
  .subtitle {
  	font-size: 40px !important;
  }
</style>


<!-- *page_number: false -->
<!-- *footer: Eigon Kimura e Kaio Duarte -->
<h1 class="title purple text-center">Projeto de mudanças no</h1>
<h1 class="title purple text-center" style="margin-top: -30px !important;" >JabRef</h1>

<h2 class="subtitle gray text-center">Engenharia de Software II<h2>

---

<h1 class="title purple">Índice</h1>
<ol class="gray" style="font-size: 35px;">
  <li>Introdução</li>
  <li>Visão geral sobre empresas juniores</li>
  <li>Documentos definidos</li>
  <li>Esclarecimentos sobre o processo</li>
</ol>

---