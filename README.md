# Escritório de projetos

## Documentos

Os documentos a seguir estarão disponíveis na forma de template (modelo) e com um exemplo preenchido, e tem como finalidade melhorar o gerenciamento de projetos de pequenos times.

- [x] Termo de abertura
- [x] Plano de gerenciamento de escopo
- [x] Plano de gerenciamento de tempo
- [ ] Plano de gerenciamento de qualidade

## Caso de exemplo

Simulação do projeto de alterações no _JabRef_.

1.  Implementar a busca de arquivo .PDF (e, opcionalmente, .PS, .ODT, .DOCX, .DOC, .PPT, .PPTX, .ODP) conforme o título do item de bibliografia em consulta.
    - Considerar que uma entrada bibtex pode ter crossref.
2.  Mostrar a quantidade de citações do documento.
3.  Mostrar o fator de impacto do local em que o documento foi publicado.

## Organização de pastas

- **templates**: Pasta com os modelos de documentos necessários para o gerenciamento do projeto.
- **exemplos**: Pasta com os modelos preenchidos baseados no caso de exemplo.
- **materiais-complementares**: Pasta com alguns casos de exemplos de projetos de outras empresas/pessoas.
