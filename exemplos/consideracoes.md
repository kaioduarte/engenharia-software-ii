# Considerações

* Semana tem 5 dias de trabalho efetivo (segunda à sexta)
* Empenho esperado por pessoa de 4 à 6h/semana
* Empenho real:
	+ Utilizando a métrica PERT:
	    - O (Otimista) : 6h semanais
	    - M (Mais provável): 4h semanais
		- P (Pessimista): 2h semanais
	+ PERT -> 4,66h (por membro, sendo 4h de trabalho efetivo e a 0.66h de reserva de contingência)
* Estimativa de preço/tempo:
	+ Considerando o valor cobrado em um estágio de TI, onde o estagiário trabalha no máximo 6h/dia e o salário é de R$ 1500:
		- 5 dias x 6 horas = 30 horas trabalhadas por semana.
		- Proporcionalmente, se 30 horas é 1, então 4 horas são 0.133 (regra de três para encontrar a proporção).
		- 0.133 indica a porção de horas trabalhadas semanalmente em um Empresa Junhor (EJ) comparada com o tempo semanal de estágio.

	+ Projetos genéricos (sites estáticos, coisas que não necessitam de coleta de requisitos à fundo e afins):
		- Consultar o preço do mercado e cobrar 70% do mesmo.

	+ Projetos pequenos (tempo <= 60h):
		- R$ 12.46/h por pessoa.

	+ Projetos médios (60h < tempo <= 112h):
		- 25% a mais do preço cobrado para projetos pequenos => R$ 15.57/h por pessoa.

	+ Projetos grandes (112h < tempo <= ...):
		- 25% a mais do preço cobrado para projetos médios => R$ 19.46/h por pessoa.

